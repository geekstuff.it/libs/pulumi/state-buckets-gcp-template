package main

import (
	stateBuckets "state-buckets-manager-lib"

	"github.com/pulumi/pulumi/sdk/v3/go/pulumi"
)

func main() {
	pulumi.Run(func(ctx *pulumi.Context) error {
		return stateBuckets.Ensure(ctx)
	})
}
